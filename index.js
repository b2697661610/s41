// Set up dependencies
const express = require("express");

const mongoose = require("mongoose");
const cors = require ("cors");

const userRoute = require("./routes/userRoute");

const courseRoute = require("./routes/courseRoute");


// Server
const app = express();

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Allows all the user routes created in "userRoute.js" file to use"/users" as route (resource)
//localhost:4000/users
app.use("/users", userRoute);



//Database connection
mongoose.connect("mongodb+srv://mervin03091996:admin123@zuitt-bootcamp.cljzexh.mongodb.net/courseBookingAPI?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
});
mongoose.connection.once("open",() => console.log(`Now connected to cloud database!`));

// Defines the "/users" string to be included for all user routes defined in the "user" route file
app.use("/users", userRoute);
// Defines the "/courses" string to be included for all user routes defined in the "course" route file
app.use("/courses", courseRoute);



//server listening
// Will used the defined port number for the application wherever environment variable is available to use port 4000 if none is defined
// this syntax will allow flexibility whem using the application locally or as a hosted application.
app.listen(process.env.Port || 4000,()=> console.log (`Now 
	connected to port ${process.env.Port ||4000}`));
